<?php

namespace App\Model;

use App\Entity\User;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
// Esta clase es creada como base para los campos de Auditoría

/**
 * @ORM\Entity()
 */
class UserManagement extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(name="user_created", referencedColumnName="id")
     */
    protected $userCreated;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(name="user_updated", referencedColumnName="id")
     */
    protected $userUpdated = NULL;


    public function __construct()
    {
        parent::__construct();
    }

    public function getUserCreated(): ?User
    {
        return $this->userCreated;
    }

    public function setUserCreated(?User $userCreated): self
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    public function getUserUpdated(): ?User
    {
        return $this->userUpdated;
    }

    public function setUserUpdated(?User $userUpdated): self
    {
        $this->userUpdated = $userUpdated;

        return $this;
    }
}
