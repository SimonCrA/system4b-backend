<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Model\UserManagement;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={"get","post"},
 *     itemOperations={
 *         "get",
 *         "put",
 *         "delete",
 *         "get_example"={
 *             "route_name"="get_example"
 *         }
 *     },
 * )
 * @ORM\Entity()
 * @ORM\Table(name="Example2s", indexes={
 *   @ORM\Index(name="fk_example2_usercreated", columns={"user_created"}),
 *   @ORM\Index(name="fk_example2_userupdated", columns={"user_updated"})
 * })
 */
class Example2 extends UserManagement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;


    public function __construct()
    {
        parent::__construct();
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
